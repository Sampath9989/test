package webdriver;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class RobotClass 
{
	WebDriver d;
	@BeforeMethod
	public void  setUp()
	{
		d=new FirefoxDriver();
		
	}
	@AfterMethod
	public void tearDown()
	{
		d.quit();
	}
	@Test
	public void testRobot() throws AWTException, InterruptedException
	{
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
		d.get("http://www.facebook.com");
		d.findElement(By.id("email")).sendKeys("sampath");
		d.findElement(By.id("pass")).sendKeys("1234");
		Robot r=new Robot();
		r.keyPress(java.awt.event.KeyEvent.VK_ENTER);
		r.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
		Thread.sleep(2000);
	}

}
