package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;



public class TestNG_Listeners implements ITestListener
{
	WebDriver d;
	@BeforeMethod
	public void setUp()
	{
		//d=new FirefoxDriver();
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_Sampath\\lib\\chromedriver.exe");
		d=new ChromeDriver();
	}
	@AfterMethod
	public void tearDown()
	{
		d.quit();
	}
	@Test
	public void testListener()
	{
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
		d.get("http://www.facebook.com");
		d.findElement(By.id("email")).sendKeys("sampat");
		d.findElement(By.id("kkk")).sendKeys("kkkk");
		d.findElement(By.id("loginbutton")).click();
	}
	@Override
	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onTestFailure(ITestResult arg0) {
		// TODO Auto-generated method stub
		System.out.println("Screen shot captured==="+arg0.toString());
		
	}
	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub
		System.out.println("Test case Started==="+arg0.toString());
		
	}
	@Override
	public void onTestSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub
		System.out.println("Test case passed===="+arg0.toString());
		
		
	}
	
	

}
