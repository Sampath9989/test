package webdriver;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class Log4j 
{
	WebDriver d;
	@Parameters({"browser"})
	@BeforeMethod
	public void  setUp(String browser)
	{
		if(browser.equals("FF"))
		{
		d=new FirefoxDriver();
		}
		else if(browser.equals("GC"))
		{
		System.setProperty("webdriver.chrome.driver", "D:\\Sam_Selenium\\Lib\\chromedriver.exe");
		d=new ChromeDriver();
		}
	}
	@AfterMethod
	public void tearDown()
	{
		d.quit();
	}
	
	@Test
	public void testRobot() throws AWTException, InterruptedException
	{
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
		Logger logger=Logger.getLogger("Facebook");
		PropertyConfigurator.configure("log4j.properties");
		
		d.get("http://www.facebook.com");
		logger.info("fb page opened");
		d.findElement(By.id("email")).sendKeys("sampath");
		logger.info("entered uname");
		d.findElement(By.id("pass")).sendKeys("1234");
		logger.info("entered pward");
		d.findElement(By.id("u_0_n")).click();
		logger.info("button clicked");
	}

}
