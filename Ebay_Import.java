package webdriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Ebay_Import 
{
	WebDriver d;
	@BeforeMethod
	public void  setUp()
	{
		d=new FirefoxDriver();
		
	}
	@AfterMethod
	public void tearDown()
	{
		d.quit();
	}
	@Test
	public void testEbay() throws BiffException, IOException, InterruptedException
	{
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
		d.get("http://www.ebay.in/");
		FileInputStream fis=new FileInputStream("D:\\Sam_Selenium\\TestData\\ebay S1.xls");
		Workbook wb=Workbook.getWorkbook(fis);
		Sheet s=wb.getSheet(0);
		s.getRow(0);
		d.findElement(By.id("gh-ac")).sendKeys(s.getCell(0, 0).getContents());
		d.findElement(By.id("gh-btn")).click();
		Thread.sleep(3000);
		
	}


}
