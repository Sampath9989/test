

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SampleProgram {

	// declaration and instantiation of objects/variables
	WebDriver driver;
	String baseUrl = "http://demo.guru99.com/V4/index.php";
	String expectedTitle = "Guru99 Bank Home Page";
	String actualTitle = "";
	String expectedManagerId = "Manger Id : mngr55729";
	String actualManagerId = "";

	@BeforeMethod
	public void setUp()
	{
		driver = new FirefoxDriver();
	}
	@Test
	public void testLogin() throws InterruptedException
	{
		// Synchronization 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// launch Firefox and direct it to the Base URL
		driver.get(baseUrl);
		Thread.sleep(4000);

		// get the actual value of the title
		actualTitle = driver.getTitle();

		// compare the actual title of the page with the expected one and print the result as "Passed" or "Failed"
		if (actualTitle.contentEquals(expectedTitle))
		{
			System.out.println("Test Passed!");
		}
		else 
		{
			System.out.println("Test Failed");
		}

		// verify whether the expected title is matches with the actual title or not..
		Assert.assertEquals(actualTitle, expectedTitle,"Expected title is not matching with the Actual title");

		// Enter UserID in UserID textbox..
		WebElement userIDTexbox = driver.findElement(By.name("uid"));
		userIDTexbox.sendKeys("mngr55729");
		Thread.sleep(2000);

		// Enter Password in Password textbox..
		WebElement passwordTextbox = driver.findElement(By.name("password"));
		passwordTextbox.sendKeys("EgarEhu");
		Thread.sleep(2000);

		// Click on Login button..
		WebElement loginButton = driver.findElement(By.name("btnLogin"));
		loginButton.click();
		Thread.sleep(2000);

		// Get the actual manager Id..
		WebElement managerId = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[3]/td"));
		actualManagerId = managerId.getText();

		// compare the Actual ManagerId of the page with the Expected ManagerId and print the result as "Passed" or "Failed"
		if (actualManagerId.contentEquals(expectedManagerId))
		{
			System.out.println("Test2 Passed!");
		}
		else 
		{
			System.out.println("Test2 Failed");
		}

		// verify whether the expected ManagerId is matches with the actual ManagerId or not..
		Assert.assertEquals(actualManagerId, expectedManagerId,"Expected ManagerId is not matching with the Actual ManagerId");
	}
	@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}


}
