package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Sample 
{
	WebDriver d;
	@BeforeMethod
	public void setUp()
	{
		d=new FirefoxDriver();
				
	}
	@AfterMethod
	public void tearDown()
	{
		d.quit();
	}
	@Test
	public void testSample() throws InterruptedException
	{
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		d.get("http://www.facebook.com");
		Thread.sleep(2000);
	}
	

}
